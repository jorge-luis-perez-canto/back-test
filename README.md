# Nabenik's Enterprise Java basic test



Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:



* Java knowledge

* DDD knowledge

* General toolkits, SDK's and other usages

* Jakarta EE general skills



To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.



This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).



## General questions



1. How to answer these questions?



> Like this



Or maybe with code



```java

fun hello() = "world"

```



2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations. Is this project using all specs?



- EJB

> EJB: Son clases en java que son desplegados en un servidor de aplicaciones y pueden ser ejecutados por una aplicación cliente.



- Servlet

> Servlet: Son clases en java que permiten procesar peticiones web por medio del protocolo http.



- CDI

> CDI: Es una especificación estándar para la inyección de dependencias que nos permite suministrar a un objeto una referencia a otros que necesite según la relación.



- JAX-RS

> JAX-RS: Es una API que permite exponer un método como un servicio RESTfull Web Service en Java EE.





3. Which of the following is an application server?



* Open Liberty

* Apache TomEE

* Eclipse Jetty

* Eclipse Glassfish

* Oracle Weblogic

> Apache TommEE, Eclipse Glassfish, Oracle Weblogic.





4. In your opinion, what's the main benefit of moving from Java 11 to Java 17?

> * Aprovechar las posibles mejoras

> * Reducir el tiempo de obsolencia

> * Mayor seguridad





5. Is it possible to run this project (as is) over Java 17? Why?

> No completamente, sin las pruebas de integración si es posible, por la incompatibilidad de las dependencias, los servidores de aplicaciones aún no son 100% compatibles compatibles con java 17 y Jakarta EE 9.





6. Is it possible to run this project with GraalVM Native? Why?


7. How do you run this project directly from CLI without configuring any application server? Why is it possible?

> Porque contiene un servidor embebido, utiliza una fuente de datos predeterminada y una estrategia de generación de tablas de bases de datos durante el despliegue.



## Development tasks



To solve this questions please use Gitflow workflow, still, your answers should be in the current branch.



Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).



0. (easy) Show your terminal demonstrating your installation of OpenJDK 11

![Image text](screenshots-on-every-task/0-jdk11.png)



1. (easy) Run this project using an IDE/Editor of your choice

![Image text](screenshots-on-every-task/1-run-ide.png)



2. (medium) Execute the movie endpoint operations using curl, if needed please also check/fix the code to be REST compliant

> create

![Image text](screenshots-on-every-task/2.1-post-create-movie.png)



> listAll Param title

![Image text](screenshots-on-every-task/2.2-get-listAll title-movie.png)



> findById

![Image text](screenshots-on-every-task/2.3.-get-findById-movie.png)



> update

![Image text](screenshots-on-every-task/2.4-put-update-movie.png)



> delete

![Image text](screenshots-on-every-task/2.5-delete-movie.png)



> delete

![Image text](screenshots-on-every-task/2.5-delete-movie.png)



> listAll

![Image text](screenshots-on-every-task/2.6-get-listAll-movie.png)



3. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer

> Link del SPA creado con React:

https://bitbucket.org/jorge-luis-perez-canto/spa-react-crud-movie/src



4. (medium) This project has been created using Java EE APIs, please move it to Jakarta EE APIs and switch it to a compatible implementation (if needed)

> Jakarta EE

![Image text](screenshots-on-every-task/4-Jakarta-EE.png)



5. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods

![Image text](screenshots-on-every-task/5-movie-repository-test.png)



6. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test

![Image text](screenshots-on-every-task/6-actor-repository-controller-model-test.png)



7. (hard) This project uses vanilla Java EE for Database Persistence, please integrate Testcontainers with MySQL for testing purposes



8. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Open Liberty](https://openliberty.io/). Do it and don't port Integration Tests 




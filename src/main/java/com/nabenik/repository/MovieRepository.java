package com.nabenik.repository;

import com.nabenik.model.Movie;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.TypedQuery;

@Stateless
@Default
 
public class MovieRepository {

    @Inject
    EntityManager em;

    // Read - find by Id
    public Movie findById(Long id) {
        return em.find(Movie.class, id);
    }

    // Create
    public void create(Movie movie) {
        em.persist(movie);
    }

    // Update
    public Movie update(long id, Movie movieData) {
        Movie movieWithId = findById(id);
        if (movieWithId != null) {
            movieData.setMovieId(movieWithId.getMovieId());
            return em.merge(movieData);
        } else {
            System.err.println("No se logro encontrar ning�n registro con ID: " + id);
            return null;
        }
    }

    // Delete
    public void delete(Long id) {
        Movie movie = findById(id);
        if (movie != null) {
            em.remove(movie);
            System.out.println("Registro eliminado.");
        } else {
            System.err.println("No se logro encontrar ning�n registro con ID: " + id);
        }
    }

    // Read - find All by title
    public List<Movie> listAll(String title) {
        String query = "SELECT m FROM Movie m WHERE m.title LIKE :title";
        TypedQuery<Movie> typedQuery = em.createQuery(query, Movie.class)
                .setParameter("title", "%".concat(title).concat("%"));
        List<Movie> resultado = typedQuery.getResultList();
        resultado.forEach(x -> System.out.println(x));
        return resultado;
    }

    // Read - find All
    public List<Movie> listAll() {
        String query = "SELECT m FROM Movie m";
        TypedQuery<Movie> typedQuery = em.createQuery(query, Movie.class);
        List<Movie> resultado = typedQuery.getResultList();
        resultado.forEach(x -> System.out.println(x));
        return resultado;
    }
}

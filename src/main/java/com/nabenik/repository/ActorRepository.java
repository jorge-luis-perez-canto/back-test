package com.nabenik.repository;

import com.nabenik.model.Actor;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.TypedQuery;

@Stateless
@Default
 
public class ActorRepository {

    @Inject
    EntityManager em;

    // Read - find by Id
    public Actor findById(Long id) {
        return em.find(Actor.class, id);
    }

    // Create
    public void create(Actor actor) {
        em.persist(actor);
    }

    // Update
    public Actor update(long id, Actor actorData) {
        Actor actorWithId = findById(id);
        if (actorWithId != null) {
            actorData.setActorId(actorWithId.getActorId());
            return em.merge(actorData);
        } else {
            System.err.println("No se logro encontrar ning�n registro con ID: " + id);
            return null;
        }
    }

    // Delete
    public void delete(Long id) {
        Actor actor = findById(id);
        if (actor != null) {
            em.remove(actor);
        } else {
            System.err.println("No se logro encontrar ning�n registro con ID: " + id);
        }
    }

    // Read - find All by name
    public List<Actor> listAll(String name) {
        String query = "SELECT a FROM Actor a WHERE a.name LIKE :name";
        TypedQuery<Actor> typedQuery = em.createQuery(query, Actor.class)
                .setParameter("name", "%".concat(name).concat("%"));
        List<Actor> resultado = typedQuery.getResultList();
        resultado.forEach(x -> System.out.println(x));
        return resultado;
    }

    // Read - find All
    public List<Actor> listAll() {
        String query = "SELECT a FROM Actor a";
        TypedQuery<Actor> typedQuery = em.createQuery(query, Actor.class);
        List<Actor> resultado = typedQuery.getResultList();
        resultado.forEach(x -> System.out.println(x));
        return resultado;
    }
}

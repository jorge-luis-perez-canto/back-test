package com.nabenik.controller;

import com.nabenik.model.Movie;
import com.nabenik.repository.MovieRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

@Path("/movies")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MovieController {

    @Inject
    MovieRepository movieRepository;

    @GET
    public List<Movie> listAll(@QueryParam("title") final String title) {
        final List<Movie> movies;
        if (title != null) {
            movies = movieRepository.listAll(title);
        } else {
            movies = movieRepository.listAll();
        }
        return movies;
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    public Movie findById(@PathParam("id") final Long id) {
        Movie movie = movieRepository.findById(id);
        return movie;
    }

    @POST
    public Response create(final Movie movie) {
        try {
            movieRepository.create(movie);
            System.out.println("Create " + movie);
            return Response.created(
                    UriBuilder.fromResource(MovieController.class)
                            .path(movie.getMovieId().toString()).build()
            ).build();
        } catch (Exception e) {
            return Response.status(Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("/{id:[0-9][0-9]*}")
    public Response update(@PathParam("id") Long id, final Movie movie) {
        Movie updatedEntity = movieRepository.update(id, movie);
        if (updatedEntity == null) {
            return Response.status(Status.NOT_FOUND).build();
        } else {
            System.out.println("Update " + updatedEntity);
        }
        return Response.accepted().build();
    }

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response delete(@PathParam("id") final Long id) {
        movieRepository.delete(id);
        return Response.ok().build();
    }
}

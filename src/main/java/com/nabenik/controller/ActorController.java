/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nabenik.controller;

import com.nabenik.model.Actor;
import com.nabenik.repository.ActorRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author root
 */
@Path("/actors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ActorController {

    @Inject
    ActorRepository actorRepository;

    @GET
    public List<Actor> listAll(@QueryParam("title") final String title) {
        final List<Actor> actors;
        if (title != null) {
            actors = actorRepository.listAll(title);
        } else {
            actors = actorRepository.listAll();
        }
        return actors;
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    public Actor findById(@PathParam("id") final Long id) {
        Actor actor = actorRepository.findById(id);
        return actor;
    }

    @POST
    public Response create(final Actor actor) {
        try {
            actorRepository.create(actor);
            System.out.println("Create " + actor);
            return Response.created(
                    UriBuilder.fromResource(ActorController.class)
                            .path(actor.getActorId().toString()).build()
            ).build();
        } catch (Exception e) {
            return Response.status(Status.BAD_REQUEST).build();
        }
    }

    @PUT
    @Path("/{id:[0-9][0-9]*}")
    public Response update(@PathParam("id") Long id, final Actor actor) {
        Actor updatedActor = actorRepository.update(id, actor);
        if (updatedActor == null) {
            return Response.status(Status.NOT_FOUND).build();
        } else {
            System.out.println("Update " + updatedActor);
        }
        return Response.accepted().build();
    }

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response delete(@PathParam("id") final Long id) {
        actorRepository.delete(id);
        return Response.ok().build();
    }
}

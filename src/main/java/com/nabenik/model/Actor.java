package com.nabenik.model;

import javax.persistence.*;
import java.io.Serializable;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Actor")
@NamedQuery(name = "Actor.findAll", query = "SELECT a FROM Actor a")
public class Actor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "actor_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long actorId;

    @Column
    @NotNull
    public String name;

    @Column
    @NotNull
    private String country;

    @Column
    @NotNull
    private String birthday;

    /*
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "movie_id", referencedColumnName = "movie_id")
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id")
    private Movie movie;

    public Actor() {
    }

    public Actor(String name, String country, String birthday) {
        this.name = name;
        this.country = country;
        this.birthday = birthday;
    }

    public Actor(String name, String country, String birthday, Movie movie) {
        this.name = name;
        this.country = country;
        this.birthday = birthday;
        this.movie = movie;
    }

    
    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Actor{" + "actorId=" + actorId + ", name=" + name + ", country=" + country + ", birthday=" + birthday + ", movie=" + movie + '}';
    }

}

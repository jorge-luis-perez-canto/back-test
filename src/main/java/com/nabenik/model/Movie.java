package com.nabenik.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Entity
@Table(name = "Movie")
@NamedQuery(name = "Movie.findAll", query = "SELECT m FROM Movie m")
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "movie_id")
    private Long movieId;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    //@Positive
    //@Min(value = 1950)
    //@Max(value = 2099)
    @Column(name = "year")
    private String year;

    @NotNull
    @Column(name = "duration")
    private String duration;

    public Movie() {

    }

    public Movie(String title, String year, String duration) {
        this.title = title;
        this.year = year;
        this.duration = duration;
    }
    
    //@OneToMany(mappedBy = "movie", fetch = FetchType.LAZY)
    @OneToMany(
            mappedBy = "movie",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Actor> actorList;

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<Actor> getActorList() {
        return actorList;
    }

    public void setActorList(List<Actor> actorList) {
        this.actorList = actorList;
    }

    @Override
    public String toString() {
        return "Movie{" + "movieId=" + movieId + ", title=" + title + ", year=" + year + ", duration=" + duration + ", actorList=" + actorList + '}';
    }

}

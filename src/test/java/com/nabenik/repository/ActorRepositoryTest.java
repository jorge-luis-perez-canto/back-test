package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import java.util.List;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;
import org.testcontainers.junit.jupiter.Testcontainers;


@RunWith(Arquillian.class)
public class ActorRepositoryTest {

    @Inject
    ActorRepository actorRepository;

    @Deployment
    public static WebArchive createDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(MovieRepository.class)
                .addClass(ActorRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");
        System.out.println(war.toString(true));
        return war;
    }

    @Test
    public void create() {
        System.out.println("\nTest create");
        //Actor actor = new Actor("Ben Affleck", "Estados Unidos", "08-15-72", new Movie("Batman v Superman", "2016", "02:31:00"));
        Actor actor = new Actor("Ben Affleck", "Estados Unidos", "08-15-72");
        actorRepository.create(actor);
        assertNotNull(actor.getActorId());
    }

    @Test
    public void findById() {
        System.out.println("\nTest findById");
        //Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72", new Movie("Batman v Superman", "2016", "02:31:00"));
        Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72");
        actorRepository.create(actor1);
        Actor actor2 = actorRepository.findById(actor1.getActorId());
        assertNotNull(actor2.getActorId());
    }

    @Test
    public void update() {
        System.out.println("\nTest update");
        //Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72", new Movie("Batman v Superman", "2016", "02:31:00"));
        Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72");
        actorRepository.create(actor1);
        //Actor updatedActor = actorRepository.update(actor1.getActorId(), new Actor("Tom Holland", "Inglaterra", "06-01-96", new Movie("Spider-Man", "2022", "148 minutos")));
        Actor updatedActor = actorRepository.update(actor1.getActorId(), new Actor("Tom Holland", "Inglaterra", "06-01-96"));
        assertEquals(updatedActor.getBirthday(), "06-01-96");
    }

    @Test
    public void delete() {
        System.out.println("\nTest delete");
        //Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72", new Movie("Batman v Superman", "2016", "02:31:00"));
        Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72");
        actorRepository.create(actor1);
        actorRepository.delete(actor1.getActorId());
        Actor actor2 = actorRepository.findById(actor1.getActorId());
        assertNull(actor2);
    }

    @Test
    public void listAllByTitle() {
        System.out.println("\nTest listAllByTitle");
        //Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72", new Movie("Batman v Superman", "2016", "02:31:00"));
        Actor actor1 = new Actor("Ben Affleck", "Estados Unidos", "08-15-72");
        actorRepository.create(actor1);
        List<Actor> listActors2 = actorRepository.listAll("Affleck");
        listActors2.forEach(x -> System.out.println(x));
        assertNotNull(listActors2);
    }
}
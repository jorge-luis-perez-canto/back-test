package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import java.util.List;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class MovieRepositoryTest {

    @Inject
    MovieRepository movieRepository;

    @Deployment
    public static WebArchive createDeployment() {
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(MovieRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");
        System.out.println(war.toString(true));
        return war;
    }

    @Test
    public void create() {
        System.out.println("\nTest create");
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 a�os");
        movieRepository.create(movie);
        assertNotNull(movie.getMovieId());
    }

    @Test
    public void findById() {
        System.out.println("\nTest findById");
        Movie movie1 = new Movie("El silencio de Jimmy", "2014", "4 a�os");
        movieRepository.create(movie1);
        Movie movie2 = movieRepository.findById(movie1.getMovieId());
        assertNotNull(movie2.getMovieId());
    }

    @Test
    public void update() {
        System.out.println("\nTest update");
        Movie movie1 = new Movie("El silencio de Jimmy", "2014", "4 a�os");
        movieRepository.create(movie1);
        Movie updatedMovie = movieRepository.update(movie1.getMovieId(), new Movie("Alejandro Giammattei Corrupci�n, Abuso de poder", "2020", "59:28:00"));
        assertEquals(updatedMovie.getYear(), "2020");
    }

    @Test
    public void delete() {
        System.out.println("\nTest delete");
        Movie movie1 = new Movie("El silencio de Jimmy", "2014", "4 a�os");
        movieRepository.create(movie1);
        movieRepository.delete(movie1.getMovieId());
        Movie movie2 = movieRepository.findById(movie1.getMovieId());
        assertNull(movie2);
    }

    @Test
    public void listAllByTitle() {
        System.out.println("\nTest listAllByTitle");
        Movie movie1 = new Movie("El silencio de Jimmy", "2014", "4 a�os");
        movieRepository.create(movie1);
        List<Movie> listMovies2 = movieRepository.listAll("Jimmy");
        listMovies2.forEach(x -> System.out.println(x));
        assertNotNull(listMovies2);
    }
}
